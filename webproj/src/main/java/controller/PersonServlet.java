package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Person;
import service.PersonService;


public class PersonServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private PersonService personService = new PersonService();

    public PersonServlet() {}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String operation = request.getParameter("operation");
		
		if( operation != null && operation.equals("display") ) {
			List<Person> listPersons = personService.findAll();
			request.setAttribute("persons", listPersons);
			this.getServletContext().getRequestDispatcher("/WEB-INF/persons.jsp").forward(request, response);
		}
		
		if( operation != null && operation.equals("update") ) {
			
			Integer id = Integer.parseInt( request.getParameter("id") );
			Person person = personService.findById(id);
			
			request.setAttribute("person", person);
			this.getServletContext().getRequestDispatcher("/WEB-INF/update.jsp").forward(request, response);
		}
		
		if( operation != null && operation.equals("delete") ) {
			
			Integer id = Integer.parseInt( request.getParameter("id") );
			Person person = personService.findById(id);
			
			personService.delete(person);
			List<Person> listPersons = personService.findAll();
			request.setAttribute("persons", listPersons);
			this.getServletContext().getRequestDispatcher("/WEB-INF/persons.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String operation = request.getParameter("operation");
		
		if( operation != null && operation.equals("create") ) {
			personService.create(new Person(request.getParameter("name")));
			this.getServletContext().getRequestDispatcher("/WEB-INF/success.jsp").forward(request, response);
		}
		
		if( operation != null && operation.equals("update") ) {
			Integer id = Integer.parseInt( request.getParameter("id") );
			String nom = request.getParameter("name");
			Person person = personService.findById(id);
			person.setUsername(nom);
			
			personService.update(person);
			this.getServletContext().getRequestDispatcher("/WEB-INF/success-update.jsp").forward(request, response);
		}
	}
}
