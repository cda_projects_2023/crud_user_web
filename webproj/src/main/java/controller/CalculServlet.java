package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.CalculOperand;


public class CalculServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public CalculServlet() {}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String operation = request.getParameter("op");
		System.out.println(operation);
		
		if( operation != null && operation.equals("addition") ) {
			
			Double a = Double.parseDouble( request.getParameter("a") );
			Double b = Double.parseDouble( request.getParameter("b") );
			Double c = a + b;
			
			CalculOperand calculOperand = new CalculOperand(a, b, c);
			
			request.setAttribute("operation", operation);
			request.setAttribute("calculOperand", calculOperand);
			this.getServletContext().getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);
		}
		
		if( operation != null && operation.equals("soustraction") ) {
			Double a = Double.parseDouble( request.getParameter("a") );
			Double b = Double.parseDouble( request.getParameter("b") );
			Double c = a - b;
			
			CalculOperand calculOperand = new CalculOperand(a, b, c);
			
			request.setAttribute("operation", operation);
			request.setAttribute("calculOperand", calculOperand);
			this.getServletContext().getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);
		}
		
		if( operation != null && operation.equals("multiplication") ) {
			Double a = Double.parseDouble( request.getParameter("a") );
			Double b = Double.parseDouble( request.getParameter("b") );
			Double c = a * b;
			
			CalculOperand calculOperand = new CalculOperand(a, b, c);
			
			request.setAttribute("operation", operation);
			request.setAttribute("calculOperand", calculOperand);
			this.getServletContext().getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);
		}
		
		if( operation != null && operation.equals("division") ) {
			Double a = Double.parseDouble( request.getParameter("a") );
			Double b = Double.parseDouble( request.getParameter("b") );
			Double c = a / b;
			
			CalculOperand calculOperand = new CalculOperand(a, b, c);
			
			request.setAttribute("operation", operation);
			request.setAttribute("calculOperand", calculOperand);
			this.getServletContext().getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);
		}
	}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
	}

}
