package dao;

import utilitary.HibernateUtilitary;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ObjectDao<T> {
	
    private Class<T> entity;
    public ObjectDao(Class<T> entity){
        this.entity = entity;
    }
    
    static Session session = HibernateUtilitary.getSessionFactory().openSession();

    public void create(T t) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(t);
            transaction.commit();
            System.out.println(transaction);
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public T findById(Integer id) {
        Transaction transaction = null;

        transaction = session.beginTransaction();
        T t = (T) session.get(entity, id);
        transaction.commit();

        return t;
    }

    public List<T> findAll() {
        Transaction transaction = session.beginTransaction();
        Criteria criteria = session.createCriteria(entity);

        List<T> t = (List<T>) criteria.list();
        transaction.commit();

        return t;
    }

    public void update(T t) {
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();



            session.saveOrUpdate(t);
            transaction.commit();
        } catch (Exception e){
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
    public void delete(T t) {
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();

            //T r = (T) session.merge(t);
            session.delete(t);
            transaction.commit();
        } catch (Exception e){
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}

