package dao.extension;

import dao.ObjectDao;
import model.Person;

public class PersonDao extends ObjectDao<Person> {
    public PersonDao(){
        super(Person.class);
    }
}

