package service;

import java.util.List;

import dao.extension.PersonDao;
import model.Person;

public class PersonService {
    private final PersonDao personDao = new PersonDao();
    
    public void create(Person person){
        personDao.create(person);
    }

    public void delete(Person person){
        personDao.delete(person);
    }

    public void update(Person person){
        personDao.update(person);
    }

    public Person findById(Integer id){
        return personDao.findById(id);
    }

    public List<Person> findAll(){
        return personDao.findAll();
    }

}