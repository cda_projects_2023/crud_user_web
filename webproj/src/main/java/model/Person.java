package model;


import javax.persistence.*;

@Entity 
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPerson;
    @Column(unique = true)
    private String username;

    public Person() {}
    
    public Person(String username) {
    	this.username = username;
    }

    public Integer getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Integer idPerson) {
        this.idPerson = idPerson;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public String toString() {
        return  System.lineSeparator() +
                "User{" +
                System.lineSeparator() +"idUser = " + idPerson +
                System.lineSeparator() +"username = " + username +
                '}'
                + System.lineSeparator();
    }
}

