package model;

public class CalculOperand {
	
	Double numberA;
	Double numberB;
	Double result;
	
	public CalculOperand(Double numberA, Double numberB, Double result) {
		super();
		this.numberA = numberA;
		this.numberB = numberB;
		this.result = result;
	}
	
	public Double getNumberA() {
		return numberA;
	}
	public void setNumberA(Double numberA) {
		this.numberA = numberA;
	}
	public Double getNumberB() {
		return numberB;
	}
	public void setNumberB(Double numberB) {
		this.numberB = numberB;
	}
	public Double getResult() {
		return result;
	}
	public void setResult(Double result) {
		this.result = result;
	}

}
