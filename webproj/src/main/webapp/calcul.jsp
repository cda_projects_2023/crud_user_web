<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" 
	  rel="stylesheet" 
	  integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" 
	  crossorigin="anonymous">
<title>calculatrice</title>
</head>

<body>

	<h1>Calculatrice</h1>

	<c:url value="http://localhost:8082/webproj/CalculServlet" var="calculLink">
		<c:param name="op" value="addition"/>
		<c:param name="op" value="soustraction"/>
		<c:param name="op" value="multiplication"/>
		<c:param name="op" value="division"/>
	</c:url>
	
	<form action="${calculLink}" method="get">
	
		<input type="number" name="a" placeholder="nombre a">
		
		<select name="op" id="op">
		    <option value="">-- Choisir un opérateur --</option>
		    <option value="addition">+</option>
		    <option value="soustraction">-</option>
		    <option value="multiplication">x</option>
		    <option value="division">:</option>
		</select>
		
		<input type="number" name="b" placeholder="nombre b">
		<input type="submit">
		
	</form>
	<br>
	
	<c:url value="index.jsp" var="indexLink"/>
	<a href="${indexLink}">retour à l'index</a>

</body>
</html>