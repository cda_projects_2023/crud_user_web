<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@ page isELIgnored="false" %>
<html>

<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" 
	  rel="stylesheet" 
	  integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" 
	  crossorigin="anonymous">
</head>
	
	<body>
		
		<h1>Home page</h1>
		
		<c:url value="register.jsp" var="registerLink"/>
		<a href="${registerLink}">enregistrer une personne</a>
		
		<br>
		
		<c:url value="/PersonServlet" var="personServlet">
			<c:param name="operation" value="display"/>
		</c:url>
		<a href="${personServlet}">liste de personnes</a>
		
		<br>
		
		<c:url value="calcul.jsp" var="calculLink"/>
		<a href="${calculLink}">calculatrice</a>
		
	</body>

</html>