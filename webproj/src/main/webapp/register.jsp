<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" 
	  rel="stylesheet" 
	  integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" 
	  crossorigin="anonymous">
<title>register</title>
</head>

<body>
	
	<h1>Enregistrer une personne</h1>
	
	<br>
	
	<c:url value="http://localhost:8082/webproj/PersonServlet" var="createLink">
		<c:param name="operation" value="create"/>
	</c:url>
	<form action="${createLink}" method="post">
		<input type="text" name="name" placeholder="entrer un nom ... ">
		<input type="submit">
	</form>
	
	<br>
	
	<c:url value="index.jsp" var="indexLink"/>
	<a href="${indexLink}">retour à l'index</a>
	
</body>
</html>