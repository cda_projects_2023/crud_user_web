<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.Person"%>

<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@page isELIgnored="false" %>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


<c:set var="listPersons" value="${ persons }" />

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" 
	  rel="stylesheet" 
	  integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" 
	  crossorigin="anonymous">
<title>liste de personnes</title>
</head>
<body>




	<h2>Liste de personnes : </h2>

	<table>

		<tr>
			<td>
			
				<c:forEach items="${listPersons}" var="p" varStatus="status">
				
					<br>
					n�: <c:out value="${status.count}"/> -
					nom : <c:out value="${p.getUsername()}"/>
					
					<c:url value="http://localhost:8082/webproj/PersonServlet" var="updateLink">
						<c:param name="operation" value="update"/>
						<c:param name="id" value="${p.getIdPerson()}"/>
					</c:url>
					<a href="${updateLink}">modifier</a>
					
					<c:url value="http://localhost:8082/webproj/PersonServlet" var="deleteLink">
						<c:param name="operation" value="delete"/>
						<c:param name="id" value="${p.getIdPerson()}"/>
					</c:url>
					<a href="${deleteLink}">supprimer</a>
					
				</c:forEach>
				
			</td>
		</tr>
		
		
		
		
	</table>
	
	<br>
	
	<a href="index.jsp">retour � l'index</a>
	
     
</body>
</html>