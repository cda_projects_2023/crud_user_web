<%@page import="model.Person"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@ page isELIgnored="false" %>
    
<c:set var="person" value="${person}"/>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" 
	  rel="stylesheet" 
	  integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" 
	  crossorigin="anonymous">
<title>update</title>
</head>
<body>

	<h2>Update </h2>
	
	<h3>modifier le nom de <c:out value="${person.getUsername()}"/></h3>
	
	<c:url value="/PersonServlet" var="updateLink">
		<c:param name="operation" value="update"/>
		<c:param name="id" value="${person.getIdPerson()}"/>
	</c:url>
	
	<form action="${updateLink}" method="post">
		<input type="text" name="name" placeholder="entrer un nouveau nom">
		<input type="submit">
	</form>
	
	<br>
	
	<c:url value="/PersonServlet" var="personServlet">
		<c:param name="operation" value="display"/>
	</c:url>
	<a href="${personServlet}">liste de personnes</a>
		
	<br>
	
	<c:url value="index.jsp" var="indexLink"/>
	<a href="${indexLink}">retour � l'index</a>
	
</body>
</html>