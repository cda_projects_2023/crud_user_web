<%@page import="model.CalculOperand"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@ page isELIgnored="false" %>
    
<c:set var="calculOperand" value="${calculOperand}"/>
<c:set var="operation" value="${operation}"/>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" 
	  rel="stylesheet" 
	  integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" 
	  crossorigin="anonymous">
<title>result</title>
</head>
<body>
	
	<h1> <c:out value="${operation}"/> </h1>

	
	<h2>Le résultat du nombre
		<c:out value="${calculOperand.getNumberA()}" />
		et du nombre
		<c:out value="${calculOperand.getNumberB()}" />
		est de :
		<c:out value="${calculOperand.getResult()}" />
		
	</h2>
	
	<c:url value="index.jsp" var="indexLink"/>
	<a href="${indexLink}">retour à l'index</a>
	
	
</body>
</html>